# Modernize Existing Java Applications
- [Objective](/platform_admins/docs/modern-java-apps.md#objective)
- [📽️ Video Walk Through](/platform_admins/docs/modern-java-apps.md#videos)
- [Architecture](/platform_admins/docs/modern-java-apps.md#Architecture)
- [Setup for Migration](/platform_admins/docs/modern-java-apps.md#setup-for-migration)
    1. [Deploy Infrastructure Foundation](/platform_admins/docs/modern-java-apps.md#1-deploy-infrastructure-foundation)
    2. [Deploy Cymbal Bank Monolith](/platform_admins/docs/modern-java-apps.md#2-deploy-cymbal-bank-monolith)
    3. [Run Linux Discovery Tool on Ledger Monolith VM](/platform_admins/docs/modern-java-apps.md#3-run-linux-discovery-tool-on-ledger-monolith-vm)
    4. [Prepare Migration Processing Cluster](/platform_admins/docs/modern-java-apps.md#4-prepare-migration-processing-cluster)
- [Doing the Migration](/platform_admins/docs/modern-java-apps.md#5-doing-the-migration)
    1. [Validate Functioning Cymbal Bank Monolith ](/platform_admins/docs/modern-java-apps.md#6-validate-functioning-cymbal-bank-monolith)
    2. [Show Migrate for Anthos Linux Discovery Tool Report](/platform_admins/docs/modern-java-apps.md#7-show-migrate-for-anthos-linux-discovery-tool-report)
    3. [Prepare and Create Migration Plan using Migrate for Anthos](/platform_admins/docs/modern-java-apps.md#8-prepare-and-create-migration-plan-using-migrate-for-anthos)
    4. [Migrate Cymbal Bank Monolith to GKE](/platform_admins/docs/modern-java-apps.md#9-migrate-cymbal-bank-monolith-to-gke)

## Objective

Modernize an existing monolithic Java application by migrating from a virtual machine to Google Kubernetes Engine.

## Videos

[📽️ Video Walk Through]() Coming Soon!

## Solution Overview & Architecture

The majority of existing, legacy applications in large enterprises are still monolithic Java-based apps running on proprietary technology stacks. The "[Modernize Existing Java Applications](https://cloud.google.com/solutions/java-app-modernization)" solution provides a systematic way to reduce operational and licensing costs while allowing enterprises to repurpose those funds to drive innovation of their Java applications. With this solution, organizations are able to incrementally modernize their existing applications by simply migrating to containers without rewriting them while reducing operational overhead. They are able to deploy, manage, and optimize all their applications—legacy as well as cloud-based—and accelerate software delivery with modern CI/CD. 

In this demo we will follow the story of Jared, an Application Operator, as he endeavors on his app modernization journey. Jared works for **Cymbal Bank**, a retail bank with a transaction processing network. Cymbal offers their customers typical online financial services such as creating/managing bank accounts and sending/receiving payments with other accounts. 

Cymbal Bank’s web-frontend, user and contact services and account database are already containerized microservices running in containers on Google Kubernetes Engine. However, their “monolithic” transaction ledger and transaction database are still running on a single virtual machine. Jared is being pressured by leadership to cut down on operational costs but he does not want to interrupt his team’s productivity.

<img src="/platform_admins/docs/img/cymbal-bank-monolith.png" width=70% height=70%>

> The steps below show a manual process of deploying a distributed service. In production, you would use an orchestrated process (and a pipeline) to deploy a distributed service. This section is for educational purposes only.

## Setup for Migration

### 1. Deploy Infrastructure Foundation
Deploy the foundation infrastructure which is a slimmed down version of the root directory [infrastructure pipeline](/README.md#deploying-the-environment) built on Cloud Build

1. Start the [Modernize Legacy Java Applications Qwiklab](https://ce.qwiklabs.com/classrooms/4865) and login to the environment in a Chrome Incognito window.
> IMPORTANT: It is highly recommended to utilize the Qwiklab environment for this demo as the resource costs can get pretty expensive if left running in your own GCP project for too long.

2. In the GCP Console, open the `Cloud Shell` environment by clicking <img src="/platform_admins/docs/img/cloud-shell-icon.png" width=10%> in the top right corner of the screen and then click <img src="/platform_admins/docs/img/cloud-shell-tab.png" width=10%> to open up the environment in a new tab. Finally, click <img src="/platform_admins/docs/img/cloud-shell-edit.png" width=7%> to open the Cloud Shell `Code Editor`.

3. Authorize your gcloud command line tool.
```bash
gcloud auth login
```

4. Set environment variables (replace `[GCP USERNAME]` with your Qwiklab user account (i.e. `student-XX-XXXXXXX@qwiklabs.net`):
```bash
export GOOGLE_PROJECT=$(gcloud config list --format 'value(core.project)')
export GCLOUD_USER=[GCP USERNAME]
```

5. Create a work directory env variable (`WORKDIR`) for this demo. All files related to the demo end up in `WORKDIR`.
```bash
mkdir -p $HOME/modern-java-apps && cd $HOME/modern-java-apps && export WORKDIR=$HOME/modern-java-apps
```

6. Clone the repository that contains the deployment automation for the demo’s infrastructure foundation:
```bash
git clone https://gitlab.com/mintindola/anthos-multicloud-workshop.git ${WORKDIR}/anthos-multicloud-workshop
```

7. Checkout the branch for this demo (less resources deployed than the full Anthos Multicloud Workshop) and pull the latest changes to your local branch:
```bash
cd ${WORKDIR}/anthos-multicloud-workshop
git config --global user.email "${GCLOUD_USER}"
git config --global user.name "qwiklab-student"

git checkout -b tech-bash
git pull origin tech-bash
```

8. Run the `build.sh` script from the root folder to set up the environment in GCP (this will take approximately 20 minutes):
```bash
./build.sh
```
>NOTE: Ensure all Cloud Build pipelines are completed successfully before proceeding. Go to the GCP Console and navigate to  Cloud Build -> History. Click <img src="/platform_admins/docs/img/cloud-build-column.png" width=13%> `Column display options` and include the `Tag` column. Wait until the following pipelines are complete: main, project-setup, dev, prod

>NOTE: If your any of your pipelines fail, watch this video on [Troubleshooting Failed Pipelines](https://app.threadit.area120.com/thread/vff54h8fep4xsxyoqkh3?utm_medium=referral-link) to learn how to resolve those issues and re-run the failing pipeline

9. Run the `user_setup.sh` script from the repository root folder:
```bash
source ${HOME}/.bashrc
cd ${WORKDIR}/anthos-multicloud-workshop
source ./user_setup.sh
```

### 2. Deploy Cymbal Bank Monolith

1. Clone the Cymbal Bank repository to your ${WORKDIR} directory:
```bash
git clone https://github.com/cloud-solutions-studio/bank-of-anthos.git ${WORKDIR}/cymbal-bank
cd ${WORKDIR}/cymbal-bank
```

2. Set environment variables to configure the deployment of Cymbal Bank with Ledger Monolith:
```bash
export PROJECT_ID=$(gcloud config list --format 'value(core.project)')
export ZONE=us-west2-a
export VM_SUBNET=prod-gcp-vpc-01-us-west2-subnet-01
export CLUSTER=gke-prod-us-west2a-1
```

3. Set `gcloud` defaults:
```bash
gcloud config set project ${PROJECT_ID}
gcloud config set compute/zone ${ZONE}
```

4. Deploy the Ledger Monolith to Google Compute Engine:
```bash
make monolith-deploy
```

5. To complete the Cymbal Bank deployment, you must configure and deploy the microservices that are already containerized and able to run on GKE (frontend, contacts, userservice, account-db):
```bash
cd ${WORKDIR}/cymbal-bank/src/ledgermonolith
```

6. Create a custom ConfigMap (config.yaml) that will tell the frontend how to reach the Ledger Monolith API endpoints:
```bash
sed -i 's/\[PROJECT_ID\]/'${PROJECT_ID}'/g' config.yaml
```

7. Navigate to the Cymbal Bank root directory and apply the Kubernetes Manifests for the remainder of microservices and their configurations:
```bash
cd ${WORKDIR}/cymbal-bank
gcloud container clusters get-credentials $CLUSTER --zone $ZONE
kubectl apply -f src/ledgermonolith/config.yaml 
kubectl apply -f extras/jwt/jwt-secret.yaml
kubectl apply -f kubernetes-manifests/accounts-db.yaml
kubectl apply -f kubernetes-manifests/userservice.yaml
kubectl apply -f kubernetes-manifests/contacts.yaml
kubectl apply -f kubernetes-manifests/frontend.yaml
kubectl apply -f kubernetes-manifests/loadgenerator.yaml

```

### 3. Run Linux Discovery Tool on Ledger Monolith VM

What is the Linux discovery tool?

Migrate for Anthos provides a self-service tool that you run on a Linux VM workload to determine the workload's fit for migration to a container. The tool outputs a score between 0 to 10, where 10 indicates a great fit for migration with little manual effort required along with a report describing the analysis results, and 0 indicates no fit or issues that must be resolved before migration.

1. SSH into “ledgermonolith-service” VM (enter ‘Y’ to continue, and then [enter] and [enter] to leave the passphrase empty):
```bash
gcloud compute ssh ledgermonolith-service --tunnel-through-iap
```

2. Create a directory for the Linux Discovery Tool's collection script and analysis tool:
```bash
mkdir m4a && cd m4a
```

3. Download the collection script to the VM and make it executable:
```bash
curl -O https://anthos-migrate-release.storage.googleapis.com/v1.8.1/linux/amd64/m4a-fit-collect.sh
chmod +x m4a-fit-collect.sh
```

4. Download the analysis tool to the VM and make it executable:
```bash
curl -O https://anthos-migrate-release.storage.googleapis.com/v1.8.1/linux/amd64/m4a-fit-analysis
chmod +x m4a-fit-analysis
```

5. Run the collect script on the VM:
```bash
sudo ./m4a-fit-collect.sh
```
The script outputs a tar file named `m4a-collect-machinename-timestamp.tar` to the current directory. The timestamp is in the format `YYYY-MM-DD-hh-mm`

6. Run the analysis tool on the tar file:
```bash
./m4a-fit-analysis m4a-collect-ledgermonolith-service-YYYY-MM-DD-hh-mm.tar
```

The tool outputs a JSON file named `analysis-report-timestamp.json` to the current directory.

7. Exit out of the SSH session:
```bash
exit
```

### 4. Prepare Migration Processing Cluster

1. Next, we will create a separate processing cluster for Migrate for Anthos as to not interfere with any running workloads on production clusters. This step takes ~2 min.
```bash
gcloud container clusters create m4a-processing \
--zone "us-west2-a" \
--network "projects/${PROJECT_ID}/global/networks/prod-gcp-vpc-01" \
--subnetwork "projects/${PROJECT_ID}/regions/us-west2/subnetworks/prod-gcp-vpc-01-us-west2-subnet-01" \
--release-channel "regular" \
--machine-type "n1-standard-4" \
--image-type "UBUNTU" \
--scopes "https://www.googleapis.com/auth/cloud-platform" \
--num-nodes "1" \
--addons HorizontalPodAutoscaling,HttpLoadBalancing
```

2. Add the processing cluster to Migrate for Anthos: In the GCP Console, go to Kubernetes Engine -> Migrate to containers. Navigate to the `Processing Clusters` tab and click `Add Processing Cluster`. 

    Step 1. Select `Linux` for the workloads type

    Step 2. Select the “m4a-processing” cluster that was just created as the

    Step 3. Run the commands listed in Cloud Shell. Then run the last command a few times until the output reads 

    ```bash
    [✓] Deployment
    [✓] Docker Registry
    [✓] Artifacts Repository
    [✗] Source Status
    No source was configured. Use 'migctl source create' to define one.
    ```
    Step 4. Click `Done`


## Doing the Migration

### 5. Validate Functioning Cymbal Bank Monolith 

1. In the GCP Console, go to  Kubernetes Engine -> Workloads. Filter by cluster `gke-prod-us-west2a-1` and namespace `default` and click on the `frontend` deployment. Scroll down and refresh until you see the Load Balancer endpoint value `(XX.XX.XX.XX:80)` under “Exposing services”. You should now be redirected to the functioning “Cymbal Bank” application. 

<img src="/platform_admins/docs/img/cymbal-bank-login.png" width=35% height=35%>
<img src="/platform_admins/docs/img/cymbal-bank-home.png" width=35% height=35%>


### 6. Show Migrate for Anthos Linux Discovery Tool Report

1. To view the output of Linux discovery tool, copy JSON result file from the Ledger MonolithVM:
```bash
gcloud compute scp --tunnel-through-iap ledgermonolith-service:~/m4a/analysis-report-* ~/
```

2. Download the Analysis Report to your local machine:
```bash
cloudshell download ~/analysis-report-*.json
```
<img src="/platform_admins/docs/img/linux-discovery-download.png" width=35% height=35%>

3. View the result file in the GCP Console: navigate to Kubernetes Engine -> Migrate to containers. Navigate to the `Fit Assessment` tab and upload the Report File by clicking "Browse" and selecting the `analysis-report-*.json` file you just downloaded to your local machine. Click "Open".

4. Under "Assessed VMs", click the `ledgermonolith-service` name to see the detailed Fit Assessment report.

<img src="/platform_admins/docs/img/linux-discovery-report.png" width=35% height=35%>


### 7. Prepare and Create Migration Plan using Migrate for Anthos

1. After you confirm that the monolithic version of “Cymbal Bank” is working, you must stop the Compute Engine instance `ledgermonolith-service` before migration. In the GCP Console, go to Compute Engine -> VM instances. Check the box next to the `ledgermonolith-service` instance and then click `Stop` in the top toolbar.

2. Back in the “Migrate to containers” screen, navigate to `Sources` tab and click `Add Source` 

    Step 1. Select `m4a-processing` as the processing cluster 

    Step 2. Name the source `ledgermonolith-source` and ensure the source type is set to `Compute Engine`

    Step 3. Ensure your project is selected as the source project and select `Create a new service account`

    Step 4. Click “Add Source”

3. Once again in the “Migrate to containers” screen, navigate to `Migrations` tab and click `Create Migration`

    Step 1. Specify the "Migration name" as `ledgermonolith-migration`

    Step 2. Select `ledgermonolith-source` as the "Source"

    Step 3. Select `Linux` as the source "VM OS type"

    Step 4. For the "Instance Name", be sure to enter the name of the source Compute Engine VM: `ledgermonolith-service`
    
    Step 5. Finally select `Image & Data` for the migration intent. 
    
    Step 6. Click `Create Migration`


### 8. Migrate Cymbal Bank Monolith to GKE

1. It will take approximately 2-3 minutes to generate the migration plan. In the “Migrate to containers” screen you will see the migration you created earlier. When the migration plan is finished, you should click `Options` in the “Next Steps” column and select `Review and edit migration plan`.

2. In the migration plan, under the `dataVolumes` field, you must replace the `<folders>`  tag with the directory on the source VM that you would like to persist to a Kubernertes PersistentVolume. We want to persist the PostgreSQL directory so replace the `<folder>` placeholder with `/var/lib/postgresql`
```bash
...
dataVolumes:  
  # Folders to include in the data volume, e.g. "/var/lib/mysql"
  # Included folders contain data and state, and therefore are automatically   excluded from a generated container image
  # Replace the placeholder with the relevant path and add more items if needed
  - folders:
    - /var/lib/postgresql
...
```

3. Click `Save and generate the artifacts` to begin the migration process. This process will take approximately 7-8 minutes.

4. When the migration is complete, you must connect to the Processing Cluster and download the artifacts to your Cloud Shell environment to deploy the migrated resources to your cluster. 
```bash
gcloud container clusters get-credentials m4a-processing --zone $ZONE --project $PROJECT_ID
```
```bash
migctl migration get-artifacts ledgermonolith-migration
```

5. Deploy the containerized `ledgermonolith-service` (along with it’s PersistentVolume and PersistentVolumeClaim) to the cluster by running:

``` bash
gcloud container clusters get-credentials $CLUSTER --zone $ZONE
```
``` bash
migctl setup install --runtime
```
```bash
kubectl apply -f deployment_spec.yaml
```

6. Finally, you must change the endpoints that the “Cymbal Bank” microservices (frontend, userservice, contacts) use to reach the `ledgermonolith-service`. They currently utilize the internal DNS address to reach the Compute Engine VM but we will switch the value to the the in-cluster service endpoint `ledgermonolith-service:8080`:
```bash
sed -i 's/'.c.${PROJECT_ID}.internal'//g' src/ledgermonolith/config.yaml
```

7. Apply the new configuration to the cluster by restarting the services to pick-up the new configuration:
```bash
kubectl apply -f ${WORKDIR}/cymbal-bank/src/ledgermonolith/config.yaml
```
```bash
kubectl delete pods --all
```

8. Ensure that the migrated Ledger Monolith running on GKE is working properly. First, ensure all Pods are running again:
```bash
kubectl get pods -w
```

Output:

```
NAME                             READY   STATUS    RESTARTS   AGE
accounts-db-0                    1/1     Running   0          35s
contacts-77db44df9b-xjx42        1/1     Running   0          37s
frontend-998f5f9d8-4nwsz         1/1     Running   0          37s
ledgermonolith-service-0         1/1     Running   0          26s
loadgenerator-5f7785d6c8-tq46p   1/1     Running   0          37s
userservice-7c77bd46cc-dvndp     1/1     Running   0          36s
```

9. In the GCP Console, go to Kubernetes Engine -> Workloads. Filter by cluster `gke-prod-us-west2a-1` and namespace `default` and click on the `frontend` deployment. Scroll down and refresh until you see the Load Balancer endpoint value `(XX.XX.XX.XX:80)` under “Exposing services”. You should now be redirected to the functioning “Cymbal Bank” application.








#### [Back to Labs](/README.md#labs)
