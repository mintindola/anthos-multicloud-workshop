#!/bin/bash
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# set -e

# Setup variables
IFS=',' read -r -a CLUSTER_NAMES <<< "${CLUSTERS_STRING}"
IFS=',' read -r -a CLUSTER_TYPES <<< "${REGIONAL_STRING}"
IFS=',' read -r -a CLUSTER_LOCS <<< "${LOCATIONS_STRING}"

# Download ASM to get istioctl
curl -LO https://storage.googleapis.com/gke-release/asm/istio-${ASM_VERSION}-linux-amd64.tar.gz
tar xzf istio-${ASM_VERSION}-linux-amd64.tar.gz
rm -rf istio-${ASM_VERSION}-linux-amd64.tar.gz
export PATH=istio-${ASM_VERSION}/bin:$PATH

curl https://storage.googleapis.com/csm-artifacts/asm/install_asm_1.9 > install_asm
chmod 755 install_asm

gsutil cp gs://${PROJECT_ID}/cicdgsa/cicd_sa_key.json .

for i in "${!CLUSTER_NAMES[@]}";
do
  # Set up envioronment
  gcloud config set project "${PROJECT_ID}"

  # Get GKE credentials
  CMD_ARG=$([ "${CLUSTER_TYPES[$i]}" = true ] && echo "--region" || echo "--zone")
  gcloud container clusters get-credentials "${CLUSTER_NAMES[$i]}" $CMD_ARG "${CLUSTER_LOCS[$i]}" --project ${PROJECT_ID}
  GKE_CTX=gke_${PROJECT_ID}_${CLUSTER_LOCS[$i]}_${CLUSTER_NAMES[$i]}
  
  # Install ASM
  ./install_asm \
    --mode install \
    --managed \
    -s cicd-sa@${PROJECT_ID}.iam.gserviceaccount.com \
    -k cicd_sa_key.json \
    -p ${PROJECT_ID} \
    -l ${CLUSTER_LOCS[$i]} \
    -n ${CLUSTER_NAMES[$i]} \
    -v \
    --output_dir ${CLUSTER_NAMES[$i]} \
    --enable-all

  istioctl install \
    --context ${GKE_CTX} \
    -f ${CLUSTER_NAMES[$i]}/managed_control_plane_gateway.yaml \
    --set revision=asm-managed \
    -y
done